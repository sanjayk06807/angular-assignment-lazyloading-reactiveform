import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path:'authentication', loadChildren:()=>import('./authentication/authentication.module').then(mod=>mod.AuthenticationModule)},
  {path:'deshboard', loadChildren:()=>import('./home/home.module').then(mod=>mod.HomeModule)}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
