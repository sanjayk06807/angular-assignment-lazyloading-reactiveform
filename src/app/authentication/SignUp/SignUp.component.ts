import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup,Validators,FormBuilder} from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-SignUp',
  templateUrl: './SignUp.component.html',
  styleUrls: ['./SignUp.component.css']
})
export class SignUpComponent implements OnInit {
  alluserdata:FormGroup;
  constructor(private router: Router, private formbuilder:FormBuilder) { 
    this.alluserdata= this.formbuilder.group({
        firstname: new FormControl('',[Validators.required ]),
        lastname: new FormControl('',[Validators.required]),
        email: new FormControl('',[Validators.required,Validators.email]),
        address: new FormControl('',[Validators.required])
      })
  }
 
// submit button start
  getalldata(){
    // console.log(this.alluserdata)
    if(this.alluserdata.valid){
   this.router.navigate(['/deshboard/analytics'])    
    }else{
      for(let i in this.alluserdata.controls)
     this.alluserdata.controls[i].markAsTouched();
    }
  }
// submit button end
  ngOnInit() {
  }
}
